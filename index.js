'use strict';
require('babel-polyfill');
const Loader = require('./dist/loader');

function run(urls, options = {}) {
    return new Loader(urls, options);
}

module.exports = {run};
