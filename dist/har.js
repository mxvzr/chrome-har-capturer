'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var url = require('url');
var querystring = require('querystring');

function create(pages) {
    // HAR template
    var packageInfo = require('../package');
    var har = {
        log: {
            version: '1.2',
            creator: {
                name: 'Chrome HAR Capturer',
                version: packageInfo.version,
                comment: packageInfo.homepage
            },
            pages: [],
            entries: []
        }
    };
    // fill the HAR template each page info
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = pages.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var _har$log$entries;

            var _ref = _step.value;

            var _ref2 = _slicedToArray(_ref, 2);

            var pageIndex = _ref2[0];
            var page = _ref2[1];

            var pageId = 'page_' + (pageIndex + 1) + '_' + String(Math.random()).slice(2);
            var log = parsePage(String(pageId), page.info);
            har.log.pages.push(log.page);
            (_har$log$entries = har.log.entries).push.apply(_har$log$entries, _toConsumableArray(log.entries));
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    return har;
}

function parsePage(pageId, page) {
    // page load started at
    var firstRequest = page.entries.get(page.firstRequestId).requestParams;
    var wallTimeMs = firstRequest.wallTime * 1000;
    var startedDateTime = new Date(wallTimeMs).toISOString();
    // page timings
    var onContentLoad = page.domContentEventFiredMs - page.firstRequestMs;
    var onLoad = page.loadEventFiredMs - page.firstRequestMs;
    // process this page load entries
    var entries = [].concat(_toConsumableArray(page.entries.values())).map(function (entry) {
        return parseEntry(pageId, entry);
    }).filter(function (entry) {
        return entry;
    });
    // outcome
    return {
        page: {
            id: pageId,
            title: page.url,
            startedDateTime: startedDateTime,
            pageTimings: {
                onContentLoad: onContentLoad,
                onLoad: onLoad
            },
            _user: page.user
        },
        entries: entries
    };
}

function parseEntry(pageref, entry) {
    // skip requests without response (requestParams is always present)
    if (!entry.responseParams || !entry.responseFinishedS && !entry.responseFailedS) {
        return null;
    }
    // skip entries without timing information (doc says optional)
    if (!entry.responseParams.response.timing) {
        return null;
    }
    // extract common fields
    var request = entry.requestParams.request;
    var response = entry.responseParams.response;
    // entry started

    var wallTimeMs = entry.requestParams.wallTime * 1000;
    var startedDateTime = new Date(wallTimeMs).toISOString();
    // HTTP version or protocol name (e.g., quic)
    var httpVersion = response.protocol || 'unknown';
    // request/response status
    var method = request.method,
        url = request.url;
    var status = response.status,
        statusText = response.statusText;
    // parse and measure headers

    var headers = parseHeaders(httpVersion, request, response);
    // check for redirections
    var redirectURL = getHeaderValue(response.headers, 'location', '');
    // parse query string
    var queryString = parseQueryString(request.url);
    // parse post data
    var postData = parsePostData(request, headers);
    // compute entry timings

    var _computeTimings = computeTimings(entry),
        time = _computeTimings.time,
        timings = _computeTimings.timings;
    // fetch connection information (strip IPv6 [...])


    var serverIPAddress = response.remoteIPAddress;
    if (serverIPAddress) {
        serverIPAddress = serverIPAddress.replace(/^\[(.*)\]$/, '$1');
    }
    var connection = String(response.connectionId);
    // fetch entry initiator
    var _initiator = entry.requestParams.initiator;
    // fetch  resource priority
    var changedPriority = entry.changedPriority;

    var newPriority = changedPriority && changedPriority.newPriority;
    var _priority = newPriority || request.initialPriority;
    // parse and measure payloads
    var payload = computePayload(entry, headers);
    var mimeType = response.mimeType;

    var encoding = entry.responseBodyIsBase64 ? 'base64' : undefined;
    // fill entry
    return {
        pageref: pageref,
        startedDateTime: startedDateTime,
        time: time,
        request: {
            method: method,
            url: url,
            httpVersion: httpVersion,
            cookies: [], // TODO
            headers: headers.request.pairs,
            queryString: queryString,
            headersSize: headers.request.size,
            bodySize: payload.request.bodySize,
            postData: postData
        },
        response: {
            status: status,
            statusText: statusText,
            httpVersion: httpVersion,
            cookies: [], // TODO
            headers: headers.response.pairs,
            redirectURL: redirectURL,
            headersSize: headers.response.size,
            bodySize: payload.response.bodySize,
            _transferSize: payload.response.transferSize,
            content: {
                size: entry.responseLength,
                mimeType: mimeType,
                compression: payload.response.compression,
                text: entry.responseBody,
                encoding: encoding
            }
        },
        cache: {},
        timings: timings,
        serverIPAddress: serverIPAddress,
        connection: connection,
        _initiator: _initiator,
        _priority: _priority
    };
}

function parseHeaders(httpVersion, request, response) {
    // convert headers from map to pairs
    var requestHeaders = response.requestHeaders || request.headers;
    var responseHeaders = response.headers;
    var headers = {
        request: {
            map: requestHeaders,
            pairs: zipNameValue(requestHeaders),
            size: -1
        },
        response: {
            map: responseHeaders,
            pairs: zipNameValue(responseHeaders),
            size: -1
        }
    };
    // estimate the header size (including HTTP status line) according to the
    // protocol (this information not available due to possible compression in
    // newer versions of HTTP)
    if (httpVersion.match(/^http\/[01].[01]$/)) {
        var requestText = getRawRequest(request, headers.request.pairs);
        var responseText = getRawResponse(response, headers.response.pairs);
        headers.request.size = requestText.length;
        headers.response.size = responseText.length;
    }
    return headers;
}

function computeTimings(entry) {
    // https://chromium.googlesource.com/chromium/blink.git/+/master/Source/devtools/front_end/sdk/HAREntry.js
    // fetch the original timing object and compute duration
    var timing = entry.responseParams.response.timing;
    var finishedTimestamp = entry.responseFinishedS || entry.responseFailedS;
    var time = toMilliseconds(finishedTimestamp - timing.requestTime);
    // compute individual components
    var blocked = firstNonNegative([timing.dnsStart, timing.connectStart, timing.sendStart]);
    var dns = -1;
    if (timing.dnsStart >= 0) {
        var start = firstNonNegative([timing.connectStart, timing.sendStart]);
        dns = start - timing.dnsStart;
    }
    var connect = -1;
    if (timing.connectStart >= 0) {
        connect = timing.sendStart - timing.connectStart;
    }
    var send = timing.sendEnd - timing.sendStart;
    var wait = timing.receiveHeadersEnd - timing.sendEnd;
    var receive = time - timing.receiveHeadersEnd;
    var ssl = -1;
    if (timing.sslStart >= 0 && timing.sslEnd >= 0) {
        ssl = timing.sslEnd - timing.sslStart;
    }
    return {
        time: time,
        timings: { blocked: blocked, dns: dns, connect: connect, send: send, wait: wait, receive: receive, ssl: ssl }
    };
}

function computePayload(entry, headers) {
    // From Chrome:
    //  - responseHeaders.size: size of the headers if available (otherwise
    //    -1, e.g., HTTP/2)
    //  - entry.responseLength: actual *decoded* body size
    //  - entry.encodedResponseLength: total on-the-wire data
    //
    // To HAR:
    //  - headersSize: size of the headers if available (otherwise -1, e.g.,
    //    HTTP/2)
    //  - bodySize: *encoded* body size
    //  - _transferSize: total on-the-wire data
    //  - content.size: *decoded* body size
    //  - content.compression: *decoded* body size - *encoded* body size
    var bodySize = void 0;
    var compression = void 0;
    var transferSize = entry.encodedResponseLength;
    if (headers.response.size === -1) {
        // if the headers size is not available (e.g., newer versions of
        // HTTP) then there is no way (?) to figure out the encoded body
        // size (see #27)
        bodySize = -1;
        compression = undefined;
    } else if (entry.responseFailedS) {
        // for failed requests (`Network.loadingFailed`) the transferSize is
        // just the header size, since that evend does not hold the
        // `encodedDataLength` field, this is performed manually (however this
        // cannot be done for HTTP/2 which is handled by the above if)
        bodySize = 0;
        compression = 0;
        transferSize = headers.response.size;
    } else {
        // otherwise the encoded body size can be obtained as follows
        bodySize = entry.encodedResponseLength - headers.response.size;
        compression = entry.responseLength - bodySize;
    }
    return {
        request: {
            // trivial case for request
            bodySize: parseInt(getHeaderValue(headers.request.map, 'content-length', -1), 10)
        },
        response: {
            bodySize: bodySize,
            transferSize: transferSize,
            compression: compression
        }
    };
}

function zipNameValue(map) {
    var pairs = [];
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
        for (var _iterator2 = Object.entries(map)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var _ref3 = _step2.value;

            var _ref4 = _slicedToArray(_ref3, 2);

            var name = _ref4[0];
            var value = _ref4[1];

            // insert multiple pairs if the key is repeated
            var values = Array.isArray(value) ? value : [value];
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = values[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var _value = _step3.value;

                    pairs.push({ name: name, value: _value });
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }
        }
    } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
            }
        } finally {
            if (_didIteratorError2) {
                throw _iteratorError2;
            }
        }
    }

    return pairs;
}

function getRawRequest(request, headerPairs) {
    var method = request.method,
        url = request.url,
        protocol = request.protocol;

    var lines = [method + ' ' + url + ' ' + protocol];
    var _iteratorNormalCompletion4 = true;
    var _didIteratorError4 = false;
    var _iteratorError4 = undefined;

    try {
        for (var _iterator4 = headerPairs[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
            var _ref5 = _step4.value;
            var name = _ref5.name;
            var value = _ref5.value;

            lines.push(name + ': ' + value);
        }
    } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion4 && _iterator4.return) {
                _iterator4.return();
            }
        } finally {
            if (_didIteratorError4) {
                throw _iteratorError4;
            }
        }
    }

    lines.push('', '');
    return lines.join('\r\n');
}

function getRawResponse(response, headerPairs) {
    var status = response.status,
        statusText = response.statusText,
        protocol = response.protocol;

    var lines = [protocol + ' ' + status + ' ' + statusText];
    var _iteratorNormalCompletion5 = true;
    var _didIteratorError5 = false;
    var _iteratorError5 = undefined;

    try {
        for (var _iterator5 = headerPairs[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
            var _ref6 = _step5.value;
            var name = _ref6.name;
            var value = _ref6.value;

            lines.push(name + ': ' + value);
        }
    } catch (err) {
        _didIteratorError5 = true;
        _iteratorError5 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion5 && _iterator5.return) {
                _iterator5.return();
            }
        } finally {
            if (_didIteratorError5) {
                throw _iteratorError5;
            }
        }
    }

    lines.push('', '');
    return lines.join('\r\n');
}

function getHeaderValue(headers, name, fallback) {
    var pattern = new RegExp('^' + name + '$', 'i');
    var key = Object.keys(headers).find(function (name) {
        return name.match(pattern);
    });
    return key === undefined ? fallback : headers[key];
}

function parseQueryString(requestUrl) {
    var _url$parse = url.parse(requestUrl, true),
        query = _url$parse.query;

    var pairs = zipNameValue(query);
    return pairs;
}

function parsePostData(request, headers) {
    var postData = request.postData;

    if (!postData) {
        return undefined;
    }
    var mimeType = getHeaderValue(headers.request.map, 'content-type');
    var params = mimeType === 'application/x-www-form-urlencoded' ? zipNameValue(querystring.parse(postData)) : [];
    return {
        mimeType: mimeType,
        params: params,
        text: postData
    };
}

function firstNonNegative(values) {
    var value = values.find(function (value) {
        return value >= 0;
    });
    return value === undefined ? -1 : value;
}

function toMilliseconds(time) {
    return time === -1 ? -1 : time * 1000;
}

module.exports = { create: create };