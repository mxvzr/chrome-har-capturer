'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CDP = require('chrome-remote-interface');

var BROWSER_TARGET = '/devtools/browser';
var VOID_URL = 'about:blank';

var Context = function () {
    function Context(options) {
        _classCallCheck(this, Context);

        this._cleanup = [];
        this._options = options;
    }

    _createClass(Context, [{
        key: 'create',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                var _this = this;

                var _options, host, port, _ref2, webSocketDebuggerUrl, browser, Target, _ref4, browserContextId, _options2, width, height, _ref6, targetId, tab;

                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                _options = this._options, host = _options.host, port = _options.port;
                                // fetch the browser version (since Chrome 62 the browser target URL is
                                // generated at runtime and can be obtained via the '/json/version'
                                // endpoint, fallback to '/devtools/browser' if not present)

                                _context5.next = 3;
                                return CDP.Version({ host: host, port: port });

                            case 3:
                                _ref2 = _context5.sent;
                                webSocketDebuggerUrl = _ref2.webSocketDebuggerUrl;
                                _context5.next = 7;
                                return CDP({
                                    host: host, port: port,
                                    target: webSocketDebuggerUrl || BROWSER_TARGET
                                });

                            case 7:
                                browser = _context5.sent;

                                this._cleanup.unshift(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                    return regeneratorRuntime.wrap(function _callee$(_context) {
                                        while (1) {
                                            switch (_context.prev = _context.next) {
                                                case 0:
                                                    _context.next = 2;
                                                    return browser.close();

                                                case 2:
                                                case 'end':
                                                    return _context.stop();
                                            }
                                        }
                                    }, _callee, _this);
                                })));
                                // request a new browser context
                                Target = browser.Target;
                                _context5.next = 12;
                                return Target.createBrowserContext();

                            case 12:
                                _ref4 = _context5.sent;
                                browserContextId = _ref4.browserContextId;

                                this._cleanup.unshift(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                    return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                        while (1) {
                                            switch (_context2.prev = _context2.next) {
                                                case 0:
                                                    _context2.next = 2;
                                                    return Target.disposeBrowserContext({ browserContextId: browserContextId });

                                                case 2:
                                                case 'end':
                                                    return _context2.stop();
                                            }
                                        }
                                    }, _callee2, _this);
                                })));
                                // create a new empty tab
                                _options2 = this._options, width = _options2.width, height = _options2.height;
                                _context5.next = 18;
                                return Target.createTarget({
                                    url: VOID_URL,
                                    width: width, height: height,
                                    browserContextId: browserContextId
                                });

                            case 18:
                                _ref6 = _context5.sent;
                                targetId = _ref6.targetId;

                                this._cleanup.unshift(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                    return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                        while (1) {
                                            switch (_context3.prev = _context3.next) {
                                                case 0:
                                                    _context3.next = 2;
                                                    return Target.closeTarget({ targetId: targetId });

                                                case 2:
                                                case 'end':
                                                    return _context3.stop();
                                            }
                                        }
                                    }, _callee3, _this);
                                })));
                                // connect to the tab and return the handler
                                _context5.next = 23;
                                return CDP({
                                    host: host, port: port,
                                    target: targetId
                                });

                            case 23:
                                tab = _context5.sent;

                                this._cleanup.unshift(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                        while (1) {
                                            switch (_context4.prev = _context4.next) {
                                                case 0:
                                                    _context4.next = 2;
                                                    return tab.close();

                                                case 2:
                                                case 'end':
                                                    return _context4.stop();
                                            }
                                        }
                                    }, _callee4, _this);
                                })));
                                return _context5.abrupt('return', tab);

                            case 26:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));

            function create() {
                return _ref.apply(this, arguments);
            }

            return create;
        }()
    }, {
        key: 'destroy',
        value: function () {
            var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                var _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, handler;

                return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                        switch (_context6.prev = _context6.next) {
                            case 0:
                                // run cleanup handlers
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context6.prev = 3;
                                _iterator = this._cleanup[Symbol.iterator]();

                            case 5:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context6.next = 12;
                                    break;
                                }

                                handler = _step.value;
                                _context6.next = 9;
                                return handler();

                            case 9:
                                _iteratorNormalCompletion = true;
                                _context6.next = 5;
                                break;

                            case 12:
                                _context6.next = 18;
                                break;

                            case 14:
                                _context6.prev = 14;
                                _context6.t0 = _context6['catch'](3);
                                _didIteratorError = true;
                                _iteratorError = _context6.t0;

                            case 18:
                                _context6.prev = 18;
                                _context6.prev = 19;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 21:
                                _context6.prev = 21;

                                if (!_didIteratorError) {
                                    _context6.next = 24;
                                    break;
                                }

                                throw _iteratorError;

                            case 24:
                                return _context6.finish(21);

                            case 25:
                                return _context6.finish(18);

                            case 26:
                            case 'end':
                                return _context6.stop();
                        }
                    }
                }, _callee6, this, [[3, 14, 18, 26], [19,, 21, 25]]);
            }));

            function destroy() {
                return _ref9.apply(this, arguments);
            }

            return destroy;
        }()
    }]);

    return Context;
}();

module.exports = Context;