'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Context = require('./context');

var Timer = function () {
    function Timer(milliseconds) {
        _classCallCheck(this, Timer);

        this._milliseconds = milliseconds;
    }

    _createClass(Timer, [{
        key: 'start',
        value: function start() {
            var _this = this;

            this.cancel();
            return new Promise(function (fulfill, reject) {
                if (typeof _this._milliseconds === 'undefined') {
                    // wait indefinitely
                    return;
                }
                _this._id = setTimeout(fulfill, _this._milliseconds);
            });
        }
    }, {
        key: 'cancel',
        value: function cancel() {
            clearTimeout(this._id);
        }
    }]);

    return Timer;
}();

var Page = function () {
    function Page(_ref) {
        var url = _ref.url,
            index = _ref.index,
            urls = _ref.urls,
            options = _ref.options;

        _classCallCheck(this, Page);

        this._url = url;
        this._index = index;
        this._urls = urls;
        this._options = options;
    }

    _createClass(Page, [{
        key: 'load',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                var _this2 = this;

                var context, client, _options, preHook, postHook, hookArgs, timer, pageLoad, disconnection, timeout;

                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                // reset page load variables
                                this.info = {
                                    url: this._url,
                                    firstRequestId: undefined,
                                    firstRequestMs: undefined,
                                    domContentEventFiredMs: undefined,
                                    loadEventFiredMs: undefined,
                                    entries: new Map(),
                                    user: undefined
                                };
                                // create a fresh new context for this URL
                                context = new Context(this._options);
                                _context4.next = 4;
                                return context.create();

                            case 4:
                                client = _context4.sent;

                                // hooks
                                _options = this._options, preHook = _options.preHook, postHook = _options.postHook;
                                hookArgs = [this._url, client, this._index, this._urls];
                                // optionally run the user-defined hook

                                if (!(typeof preHook === 'function')) {
                                    _context4.next = 10;
                                    break;
                                }

                                _context4.next = 10;
                                return preHook.apply(null, hookArgs);

                            case 10:
                                // create (but not start) the page timer
                                timer = new Timer(this._options.timeout);
                                // handle proper page load and postHook or related errors

                                pageLoad = function () {
                                    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                        return regeneratorRuntime.wrap(function _callee$(_context) {
                                            while (1) {
                                                switch (_context.prev = _context.next) {
                                                    case 0:
                                                        _context.prev = 0;
                                                        _context.next = 3;
                                                        return _this2._loadPage(client);

                                                    case 3:
                                                        if (!(typeof postHook === 'function')) {
                                                            _context.next = 7;
                                                            break;
                                                        }

                                                        _context.next = 6;
                                                        return postHook.apply(null, hookArgs);

                                                    case 6:
                                                        _this2.info.user = _context.sent;

                                                    case 7:
                                                        _context.prev = 7;
                                                        _context.next = 10;
                                                        return context.destroy();

                                                    case 10:
                                                        timer.cancel();
                                                        return _context.finish(7);

                                                    case 12:
                                                    case 'end':
                                                        return _context.stop();
                                                }
                                            }
                                        }, _callee, _this2, [[0,, 7, 12]]);
                                    }));

                                    return function pageLoad() {
                                        return _ref3.apply(this, arguments);
                                    };
                                }();
                                // handle Chrome disconnection


                                disconnection = function () {
                                    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                            while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                    case 0:
                                                        _context2.next = 2;
                                                        return new Promise(function (fulfill, reject) {
                                                            client.once('disconnect', fulfill);
                                                        });

                                                    case 2:
                                                        timer.cancel();
                                                        throw new Error('Disconnected');

                                                    case 4:
                                                    case 'end':
                                                        return _context2.stop();
                                                }
                                            }
                                        }, _callee2, _this2);
                                    }));

                                    return function disconnection() {
                                        return _ref4.apply(this, arguments);
                                    };
                                }();
                                // handle page timeout


                                timeout = function () {
                                    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                            while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                    case 0:
                                                        _context3.next = 2;
                                                        return timer.start();

                                                    case 2:
                                                        _context3.next = 4;
                                                        return context.destroy();

                                                    case 4:
                                                        throw new Error('Timed out');

                                                    case 5:
                                                    case 'end':
                                                        return _context3.stop();
                                                }
                                            }
                                        }, _callee3, _this2);
                                    }));

                                    return function timeout() {
                                        return _ref5.apply(this, arguments);
                                    };
                                }();
                                // wait for the first event to happen


                                _context4.next = 16;
                                return Promise.race([pageLoad(), disconnection(), timeout()]);

                            case 16:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function load() {
                return _ref2.apply(this, arguments);
            }

            return load;
        }()
    }, {
        key: '_loadPage',
        value: function () {
            var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(client) {
                var _this3 = this;

                var Page, Network, termination, navigation;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                // enable domains
                                Page = client.Page, Network = client.Network;
                                _context5.next = 3;
                                return Network.enable({
                                    maxPostDataSize: Number(this._options.postData) || undefined
                                });

                            case 3:
                                _context5.next = 5;
                                return Page.enable();

                            case 5:
                                // register events synchronously
                                termination = new Promise(function (fulfill, reject) {
                                    _this3._processEvents(client, fulfill, reject);
                                });
                                // start the page load

                                navigation = Page.navigate({ url: this._url });
                                // events will determine termination

                                _context5.next = 9;
                                return Promise.all([termination, navigation]);

                            case 9:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));

            function _loadPage(_x) {
                return _ref6.apply(this, arguments);
            }

            return _loadPage;
        }()
    }, {
        key: '_processEvents',
        value: function _processEvents(client, fulfill, reject) {
            var _this4 = this;

            var info = this.info;
            var Page = client.Page,
                Network = client.Network;


            Page.domContentEventFired(function (_ref7) {
                var timestamp = _ref7.timestamp;

                info.domContentEventFiredMs = timestamp * 1000;
                // check termination condition
                _this4._checkFinished(fulfill);
            });

            Page.loadEventFired(function (_ref8) {
                var timestamp = _ref8.timestamp;

                info.loadEventFiredMs = timestamp * 1000;
                // check termination condition
                _this4._checkFinished(fulfill);
            });

            Network.requestWillBeSent(function (params) {
                var requestId = params.requestId,
                    initiator = params.initiator,
                    timestamp = params.timestamp,
                    redirectResponse = params.redirectResponse;
                // skip data URI

                if (params.request.url.match('^data:')) {
                    return;
                }
                // the first is the first request
                if (!info.firstRequestId && initiator.type === 'other') {
                    info.firstRequestMs = timestamp * 1000;
                    info.firstRequestId = requestId;
                }
                // redirect responses are delivered along the next request
                if (redirectResponse) {
                    var redirectEntry = info.entries.get(requestId);
                    // craft a synthetic response params
                    redirectEntry.responseParams = {
                        response: redirectResponse
                    };
                    // set the redirect response finished when the redirect
                    // request *will be sent* (this may be an approximation)
                    redirectEntry.responseFinishedS = timestamp;
                    redirectEntry.encodedResponseLength = redirectResponse.encodedDataLength;
                    // since Chrome uses the same request id for all the
                    // redirect requests, it is necessary to disambiguate
                    var newId = requestId + '_redirect_' + timestamp;
                    // rename the previous metadata entry
                    info.entries.set(newId, redirectEntry);
                    info.entries.delete(requestId);
                }
                // initialize this entry
                info.entries.set(requestId, {
                    requestParams: params,
                    responseParams: undefined,
                    responseLength: 0, // built incrementally
                    encodedResponseLength: undefined,
                    responseFinishedS: undefined,
                    responseFailedS: undefined,
                    responseBody: undefined,
                    responseBodyIsBase64: undefined,
                    newPriority: undefined
                });
                // check termination condition
                _this4._checkFinished(fulfill);
            });

            Network.dataReceived(function (_ref9) {
                var requestId = _ref9.requestId,
                    dataLength = _ref9.dataLength;

                var entry = info.entries.get(requestId);
                if (!entry) {
                    return;
                }
                entry.responseLength += dataLength;
            });

            Network.responseReceived(function (params) {
                var entry = info.entries.get(params.requestId);
                if (!entry) {
                    return;
                }
                entry.responseParams = params;
            });

            Network.resourceChangedPriority(function (_ref10) {
                var requestId = _ref10.requestId,
                    newPriority = _ref10.newPriority;

                var entry = info.entries.get(requestId);
                if (!entry) {
                    return;
                }
                entry.newPriority = newPriority;
            });

            Network.loadingFinished(function () {
                var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(_ref11) {
                    var requestId = _ref11.requestId,
                        timestamp = _ref11.timestamp,
                        encodedDataLength = _ref11.encodedDataLength;
                    var entry, params, body, base64Encoded;
                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                        while (1) {
                            switch (_context6.prev = _context6.next) {
                                case 0:
                                    entry = info.entries.get(requestId);

                                    if (entry) {
                                        _context6.next = 3;
                                        break;
                                    }

                                    return _context6.abrupt('return');

                                case 3:
                                    entry.encodedResponseLength = encodedDataLength;
                                    entry.responseFinishedS = timestamp;
                                    // optionally fetch the entry content

                                    if (!_this4._options.content) {
                                        _context6.next = 19;
                                        break;
                                    }

                                    _context6.prev = 6;
                                    _context6.next = 9;
                                    return Network.getResponseBody({ requestId: requestId });

                                case 9:
                                    params = _context6.sent;
                                    body = params.body, base64Encoded = params.base64Encoded;

                                    entry.responseBody = body;
                                    entry.responseBodyIsBase64 = base64Encoded;
                                    _context6.next = 19;
                                    break;

                                case 15:
                                    _context6.prev = 15;
                                    _context6.t0 = _context6['catch'](6);

                                    reject(_context6.t0);
                                    return _context6.abrupt('return');

                                case 19:
                                case 'end':
                                    return _context6.stop();
                            }
                        }
                    }, _callee6, _this4, [[6, 15]]);
                }));

                return function (_x2) {
                    return _ref12.apply(this, arguments);
                };
            }());

            Network.loadingFailed(function (_ref13) {
                var requestId = _ref13.requestId,
                    errorText = _ref13.errorText,
                    canceled = _ref13.canceled,
                    timestamp = _ref13.timestamp;

                // abort the whole page if the first request fails
                if (requestId === info.firstRequestId) {
                    var message = errorText || canceled && 'Canceled';
                    reject(new Error(message));
                }
                // otherwise just mark the resource as failed
                var entry = info.entries.get(requestId);
                if (!entry) {
                    return;
                }
                entry.responseFailedS = timestamp;
            });
        }
    }, {
        key: '_checkFinished',
        value: function _checkFinished(fulfill) {
            var info = this.info;
            // a page is considered 'finished' when all these three messages
            // arrived: a reply to the first request, Page.domContentEventFired and
            // Page.loadEventFired

            if (info.firstRequestMs && info.domContentEventFiredMs && info.loadEventFiredMs) {
                fulfill();
            }
        }
    }]);

    return Page;
}();

module.exports = Page;