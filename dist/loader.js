'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HAR = require('./har');
var Page = require('./page');

var EventEmitter = require('events');

var Loader = function (_EventEmitter) {
    _inherits(Loader, _EventEmitter);

    function Loader(urls, options) {
        _classCallCheck(this, Loader);

        var _this = _possibleConstructorReturn(this, (Loader.__proto__ || Object.getPrototypeOf(Loader)).call(this));

        _this._urls = urls;
        _this._options = options;
        // continue in the next tick to allow event registration
        process.nextTick(function () {
            _this._run();
        });
        return _this;
    }

    _createClass(Loader, [{
        key: '_run',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var pages, har;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                if (!this._options.parallel) {
                                    _context.next = 6;
                                    break;
                                }

                                _context.next = 3;
                                return this._runConcurrently();

                            case 3:
                                _context.t0 = _context.sent;
                                _context.next = 9;
                                break;

                            case 6:
                                _context.next = 8;
                                return this._runSequentially();

                            case 8:
                                _context.t0 = _context.sent;

                            case 9:
                                pages = _context.t0;

                                // build and return the HAR file
                                har = HAR.create(pages.filter(function (page) {
                                    return !!page;
                                }));

                                this.emit('har', har);

                            case 12:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function _run() {
                return _ref.apply(this, arguments);
            }

            return _run;
        }()
    }, {
        key: '_runSequentially',
        value: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var pages, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _ref3, _ref4, index, url;

                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                        switch (_context2.prev = _context2.next) {
                            case 0:
                                pages = [];
                                _iteratorNormalCompletion = true;
                                _didIteratorError = false;
                                _iteratorError = undefined;
                                _context2.prev = 4;
                                _iterator = this._urls.entries()[Symbol.iterator]();

                            case 6:
                                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                                    _context2.next = 19;
                                    break;
                                }

                                _ref3 = _step.value;
                                _ref4 = _slicedToArray(_ref3, 2);
                                index = _ref4[0];
                                url = _ref4[1];
                                _context2.t0 = pages;
                                _context2.next = 14;
                                return this._handleUrl(url, index);

                            case 14:
                                _context2.t1 = _context2.sent;

                                _context2.t0.push.call(_context2.t0, _context2.t1);

                            case 16:
                                _iteratorNormalCompletion = true;
                                _context2.next = 6;
                                break;

                            case 19:
                                _context2.next = 25;
                                break;

                            case 21:
                                _context2.prev = 21;
                                _context2.t2 = _context2['catch'](4);
                                _didIteratorError = true;
                                _iteratorError = _context2.t2;

                            case 25:
                                _context2.prev = 25;
                                _context2.prev = 26;

                                if (!_iteratorNormalCompletion && _iterator.return) {
                                    _iterator.return();
                                }

                            case 28:
                                _context2.prev = 28;

                                if (!_didIteratorError) {
                                    _context2.next = 31;
                                    break;
                                }

                                throw _iteratorError;

                            case 31:
                                return _context2.finish(28);

                            case 32:
                                return _context2.finish(25);

                            case 33:
                                return _context2.abrupt('return', pages);

                            case 34:
                            case 'end':
                                return _context2.stop();
                        }
                    }
                }, _callee2, this, [[4, 21, 25, 33], [26,, 28, 32]]);
            }));

            function _runSequentially() {
                return _ref2.apply(this, arguments);
            }

            return _runSequentially;
        }()
    }, {
        key: '_runConcurrently',
        value: function () {
            var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                var _this2 = this;

                var pages, degree, index, worker;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                        switch (_context4.prev = _context4.next) {
                            case 0:
                                pages = [];
                                degree = Number(this._options.parallel);
                                index = 0;

                                worker = function () {
                                    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                        var url, page;
                                        return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                            while (1) {
                                                switch (_context3.prev = _context3.next) {
                                                    case 0:
                                                        if (!(index < _this2._urls.length)) {
                                                            _context3.next = 8;
                                                            break;
                                                        }

                                                        url = _this2._urls[index];
                                                        _context3.next = 4;
                                                        return _this2._handleUrl(url, index++);

                                                    case 4:
                                                        page = _context3.sent;

                                                        pages.push(page);
                                                        _context3.next = 0;
                                                        break;

                                                    case 8:
                                                    case 'end':
                                                        return _context3.stop();
                                                }
                                            }
                                        }, _callee3, _this2);
                                    }));

                                    return function worker() {
                                        return _ref6.apply(this, arguments);
                                    };
                                }();
                                // spawn workers


                                _context4.next = 6;
                                return Promise.all(new Array(degree).fill().map(worker));

                            case 6:
                                return _context4.abrupt('return', pages);

                            case 7:
                            case 'end':
                                return _context4.stop();
                        }
                    }
                }, _callee4, this);
            }));

            function _runConcurrently() {
                return _ref5.apply(this, arguments);
            }

            return _runConcurrently;
        }()
    }, {
        key: '_handleUrl',
        value: function () {
            var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(url, index) {
                var _this3 = this;

                var triesLeft = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this._options.retry || 0;
                var page;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                page = new Page({
                                    url: url, index: index,
                                    urls: this._urls,
                                    options: this._options
                                });
                                _context5.prev = 1;

                                this.emit('load', url, index, this._urls);
                                _context5.next = 5;
                                return page.load();

                            case 5:
                                this.emit('done', url, index, this._urls);
                                _context5.next = 18;
                                break;

                            case 8:
                                _context5.prev = 8;
                                _context5.t0 = _context5['catch'](1);

                                this.emit('fail', url, _context5.t0, index, this._urls);

                                if (!(triesLeft > 0)) {
                                    _context5.next = 17;
                                    break;
                                }

                                _context5.next = 14;
                                return new Promise(function (fulfill, reject) {
                                    setTimeout(fulfill, _this3._options.retryDelay || 0);
                                });

                            case 14:
                                return _context5.abrupt('return', this._handleUrl(url, index, triesLeft - 1));

                            case 17:
                                return _context5.abrupt('return', null);

                            case 18:
                                return _context5.abrupt('return', page);

                            case 19:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this, [[1, 8]]);
            }));

            function _handleUrl(_x2, _x3) {
                return _ref7.apply(this, arguments);
            }

            return _handleUrl;
        }()
    }]);

    return Loader;
}(EventEmitter);

module.exports = Loader;